from tkinter import *
import tkinter.font as tkFont
from random import *
import getopt

def opt_help():
    print("  -a\t--afforestation= \tDefine afforestation percentage, must be >= 0. Default : 0.6") 
    print("  -c\t--cols= \t\tDefine the number of columns, must be >= 0. Default : 80") 
    print("  -h\t--help \t\t\tDisplay the help and exit") 
    print("  -r\t--rows= \t\tDefine the number of rows, must be >= 0. Default : 50") 
    print("  -s\t--cell_size= \t\tDefine size for cells, must be >= 0. Default : 10") 

def main(argv):
    rows = 50
    columns = 80
    cell_size = 10
    afforestation_percentage = 0.6

    try:                                
        opts, args = getopt.getopt(argv, "hr:c:s:a:", ["help", "rows=", "cols=", "cell_size=", "afforestation="])
    except getopt.GetoptError:                                   
        sys.exit(2)                     
    for opt, arg in opts: 
        if opt in ("-h", "--help"):      
            opt_help() 
            sys.exit(0)                
        elif opt in ("-r", "--rows") and int(arg)>0:      
            rows = int(arg)                                      
        elif opt in ("-c", "--cols") and int(arg)>0:                
            columns = int(arg)                
        elif opt in ("-s", "--cell_size") and int(arg)>0: 
            cell_size = int(arg) 
        elif opt in ("-a", "--afforestation") and float(arg)>=0: 
            afforestation_percentage = float(arg)    

    window = Tk()

    rule = IntVar(master=window, value=1)
    play = IntVar(master=window)
    font_style = tkFont.Font(root=window, family='Helvetica', size=14)

    window.title("Feu de foret")
    width, height = columns*cell_size + 20, rows*cell_size + 120
    window.geometry("%dx%d" % (width, height))
    window.configure(background='#545bcb')
    window.resizable(width=False, height=False)


    def quit_callback():
        window.destroy()
        sys.exit(0)


    canvas = Canvas(window, width=columns*cell_size, height=rows*cell_size, background='black', highlightthickness=0)
    canvas.grid(row=0, column=0, columnspan=3, pady=10, padx=10)

    btn_play = Checkbutton(window, text= "Play", font=font_style, background='#a2a3b0', 
                        activebackground='#a3a6cd', selectcolor='#eeeff6', highlightthickness=0, 
                        indicatoron=0, variable=play)
    btn_play.grid(row = 1, column=0, pady=2, padx=10, sticky=N+S+E+W)

    btn_quit = Button(window, text= "Quit", font=font_style, background='#a2a3b0', 
                    activebackground='#a3a6cd', highlightthickness=0, command=quit_callback)
    btn_quit.grid(row = 1, column=2, pady=2, padx=10, sticky=N+S+E+W)

    lbl_rule = Label(window, text="Rule choice : ", background='#545bcb', font=font_style)
    lbl_rule.grid(row = 2, column=0, pady=2, padx=10, sticky=N+S+E+W)

    btn_rule1 = Radiobutton(window, text="Rule 1", variable = rule, value = 1, indicatoron=0, 
                            background='#a2a3b0', selectcolor='#eeeff6', activebackground='#a3a6cd', 
                            font=font_style, highlightthickness=0)
    btn_rule2 = Radiobutton(window, text="Rule 2", variable = rule, value = 2, indicatoron=0, 
                            background='#a2a3b0', selectcolor='#eeeff6', activebackground='#a3a6cd', 
                            font=font_style, highlightthickness=0)
    btn_rule1.grid(row = 2, column = 1, pady=2, padx=10, sticky=N+S+E+W)
    btn_rule2.grid(row = 2, column = 2, pady=2, padx=10, sticky=N+S+E+W)
    btn_rule1.select()

    for x in range(3):
        Grid.columnconfigure(window, x, weight=1)

    for y in range(3):
        Grid.rowconfigure(window, y, weight=1)

    #   0 -> plain
    #   1 -> forest
    #   2 -> fire
    #   3 -> ashes
    def new_grid(nb_lines, nb_columns):
        grid = [[]] * nb_lines
        for line in range(nb_lines):
            grid[line] = [0] * nb_columns
        return grid


    def init_forest(grid, percentage):
        for line in range(len(grid)):
            for column in range(len(grid[0])):
                if random() <= percentage:
                    grid[line][column]=1


    def reset_forest(grid):
        for line in range(len(grid)):
            for column in range(len(grid[0])):
                    grid[line][column]=0


    def check_neighbor(grid, line, column):
        neighbors = [0] * 4
        if line+1 < len(grid):
            neighbors[grid[line+1][column]] += 1
        if line-1 >= 0:
            neighbors[grid[line-1][column]] += 1
        if column+1 < len(grid[0]):
            neighbors[grid[line][column+1]] += 1
        if column-1 >=0:
            neighbors[grid[line][column-1]] += 1

        return neighbors


    def new_rectangle(pos_x, pos_y, cell_type):
        color = ''
        type_name = ''
        if cell_type == 1:
            color = 'green'
            type_name = "forest"
        elif cell_type == 2:
            color = 'red'
            type_name = "fire"
        elif cell_type == 3:
            color = 'gray'
            type_name = "ashes"
        else:
            color = 'black'
        canvas.create_rectangle(pos_x * cell_size, pos_y * cell_size, (pos_x + 1) * cell_size,
                                (pos_y + 1) * cell_size, fill=color, tag=type_name, outline="")


    def update_canvas(grid):
        canvas.delete('forest')
        canvas.delete('fire')
        canvas.delete('ashes')
        for line in range(len(grid)):
            for column in range(len(grid[0])):
                if grid[line][column]:
                    new_rectangle(column, line, grid[line][column])


    def update_forest(grid, rule):
        next_grid = new_grid(len(grid), len(grid[0]))
        for line in range(len(grid)):
            for column in range(len(grid[0])):
                next_grid[line][column] = grid[line][column]
                if grid[line][column] == 2:
                    next_grid[line][column] = 3
                if grid[line][column] == 3:
                    next_grid[line][column] = 0
                if rule.get() == 1:
                    if grid[line][column] == 1 and check_neighbor(grid,line,column)[2] >0:
                        next_grid[line][column] = 2
                else: 
                    if grid[line][column] == 1 and random() < 1-1/(check_neighbor(grid,line,column)[2] +1):
                        next_grid[line][column] = 2


        grid[:]= next_grid[:]


    forest = new_grid(rows,columns)
    init_forest(forest, afforestation_percentage)

    def new_forest():
        reset_forest(forest)
        init_forest(forest,afforestation_percentage)
        update_canvas(forest)
    btn_new_forest = Button(window, text= "New Forest", font=font_style, background='#a2a3b0', 
                            activebackground='#a3a6cd', highlightthickness=0, command=new_forest)
    btn_new_forest.grid(row = 1, column=1, pady=2, padx=10, sticky=N+S+E+W)


    def click_callback(event, grid=forest):
        x1 = event.x // cell_size
        y1 = event.y // cell_size
        if grid[y1][x1] == 1:
            grid[y1][x1] = 2
            update_canvas(grid)


    def right_click_callback(event, grid=forest):
        x1 = event.x // cell_size
        y1 = event.y // cell_size
        if grid[y1][x1] == 2:
            grid[y1][x1] = 1
            update_canvas(grid)

        
    canvas.bind('<Button-1>', click_callback)
    canvas.bind('<Button-3>', right_click_callback)

    def anim():
        if play.get():
            update_forest(forest, rule)
            update_canvas(forest)
        window.after(200, anim)

    update_canvas(forest)
    anim()
    window.mainloop()


if __name__ == "__main__":
    main(sys.argv[1:])