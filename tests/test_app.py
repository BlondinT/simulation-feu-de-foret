import unittest
from app import *

class TestAppFunctions(unittest.TestCase):
    
    def test_new_grid(self):
        list_test = [[0,0,0],[0,0,0]]
        list_function = new_grid(2,3)
        self.assertEqual(len(list_test), len(list_function))
        self.assertEqual(len(list_test[0]), len(list_function[0]))

    def test_init_forest(self):
        forest_test = [[1,1],[1,1]]
        forest_function = new_grid(2,2)
        init_forest(forest_function, 1)

        forest_test2 = new_grid(2,2)
        forest_function2 = new_grid(2,2)
        init_forest(forest_function, 0)

        for line in range(2):
            for column in range(2):
                self.assertEqual(forest_test[line][column], forest_function[line][column])
                self.assertEqual(forest_test2[line][column], forest_function2[line][column])

    
    def test_update_forest(self):
        forest_test = [[1,0]]
        forest_res = [[1,0]]
        update_forest(forest_test,1)
        for line in range(2):
            self.assertEqual(forest_test[0][line], forest_res[0][line])

        forest_test = [[2,1]]
        forest_res = [[3,2]]
        update_forest(forest_test,1)
        for line in range(2):
            self.assertEqual(forest_test[0][line], forest_res[0][line])
        
        forest_test = [[3,2]]
        forest_res = [[0,3]]
        update_forest(forest_test,1)
        for line in range(2):
            self.assertEqual(forest_test[0][line], forest_res[0][line])